import React, { useState } from 'react';
import NewCounterForm from "./components/counter/NewCounterForm";
import CounterList from "./components/counter/CounterList";
import TimerList from "./components/timer/TimerList";


const App1 = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, showLoader] = useState(false);
  const [error, setError] = useState('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const [counterIncrements, setCounterIncrements] = useState([]);

  const login = async () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("USERNAME: ", username)
        console.log("PASSWORD: ", password)
        if (username === 'qstation' && password === 'pass1') {
          resolve();
        } else {
          reject();
        }
      }, 5000);
    });
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    setError('');
    showLoader(true);
    try {
      await login();
      setIsLoggedIn(true);
    } catch (error) {
      setError('Incorrect username or password!');
      showLoader(false);
      setUsername('');
      setPassword('');
    }
  };


  const addCounterValueIncrement = (incrementStep) => {
    setCounterIncrements(prevState => [...prevState, incrementStep]);
  }

  return (
    <div className='App'>
      <div className='login-container'>
        {isLoggedIn ? (
              <div>
              <NewCounterForm onAddCounterIncrement={addCounterValueIncrement} />
              <CounterList counterIncrements={counterIncrements} />
              <TimerList />
            </div>
        ) : (
          <form className='form' onSubmit={onSubmit}>
            {error && <p className='error'>{error}</p>}
            <p>Please Login!</p>
            <input
              type='text'
              placeholder='username'
              value={username}
              onChange={(e) => setUsername(e.currentTarget.value)}
            />
            <input
              type='password'
              placeholder='password'
              autoComplete='new-password'
              value={password}
              onChange={(e) => setPassword(e.currentTarget.value)}
            />
            <button className='submit' type='submit' disabled={isLoading}>
              {isLoading ? 'Logging in...' : 'Log In'}
            </button>
          </form>
        )}
      </div>
    </div>
  );
}

export default App1;