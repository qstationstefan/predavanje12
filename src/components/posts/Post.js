const Post = (props) => {
    return (
        <div>
            <h2>{props.title}</h2>
            <p>{props.data}</p>
        </div>
    )
}

export default Post