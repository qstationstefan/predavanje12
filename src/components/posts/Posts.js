import { useEffect, useState } from "react";
import Post from "./Post";

const Posts = () => {
    const [posts, setPosts] = useState([])

    const fetchPostsHandler = async () => {
        try {
            const response = await fetch("https://jsonplaceholder.typicode.com/posts");
            if (response.status === 200) {
                const data = await response.json();
                console.log(data)
                const transformedData = data.map(item => {
                    return {
                        id: item.id,
                        data: item.body,
                        title: item.title
                    }
                });
                setPosts(transformedData);
            } else if(response.status === 401) {
                console.log("Izbaci login modal")
            } else {
                console.log("Greska")
            }
        } catch (error) {
            console.log("DOSLO JE DO GRESKE")
        }
    }

    // const fetchPostsHandler = () => {
    //     fetch("https://jsonplaceholder.typicode.com/posts").
    //         then(response => response.json()).
    //         then(data => {
    //             console.log(data)
    //             const transformedData = data.map(item => {
    //                 return {
    //                     id: item.id,
    //                     data: item.body,
    //                     title: item.title
    //                 }
    //             });
    //             setPosts(transformedData);
    //         }).catch(error => console.log("Doslo je do greske."))
    // }

    useEffect(() => {
        fetchPostsHandler();
    }, [])

    return (
        <div>
            <h1>Posts:</h1>
            <button onClick={fetchPostsHandler}>Fetch posts</button>
            {posts.map(item => <Post key={item.id} title={item.title} data={item.data} />)}
        </div>
    )
}

export default Posts