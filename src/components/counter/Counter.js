import { useReducer } from "react";

const INCREMENT_ONE = "increment_one";
const INCREMENT_N = "increment_n";
const DECREMENT_ONE = "decrement_one";
const DECREMENT_N = "decrement_n";
const RESET = "reset";

const initialState = {
  counterValue: 0,
  incrementActionsCount: 0,
  decrementActionsCount: 0,
};

const reducer = (state, action) => {
  switch (action.type) {
    case INCREMENT_ONE:
      return {
        ...state,
        counterValue: state.counterValue + 1,
        incrementActionsCount: state.incrementActionsCount + 1,
      };
    case INCREMENT_N:
      return {
        ...state,
        counterValue: state.counterValue + action.payload,
        incrementActionsCount: state.incrementActionsCount + 1,
      };
    case DECREMENT_ONE:
      return {
        ...state,
        counterValue: state.counterValue - 1,
        decrementActionsCount: state.decrementActionsCount + 1,
      };
    case DECREMENT_N:
      return {
        ...state,
        counterValue: state.counterValue - action.payload,
        decrementActionsCount: state.decrementActionsCount + 1,
      };
    case RESET: 
      return initialState;
    default:
      return state;
  }
};

const Counter = (props) => {
  const [counter, dispatch] = useReducer(reducer, initialState);

  const incrementOneHandler = () => {
    dispatch({ type: INCREMENT_ONE });
  };

  const incrementNHandler = () => {
    dispatch({ type: INCREMENT_N, payload: props.incrementStep });
  };

  const decrementOneHandler = () => {
    dispatch({ type: DECREMENT_ONE });
  };

  const decrementNHandler = () => {
    dispatch({ type: DECREMENT_N, payload: props.incrementStep });
  };

  return (
    <div>
      <h2>Counter</h2>
      <div>{`Increment actions count: ${counter.incrementActionsCount}`}</div>
      <div>{`Decrement actions count: ${counter.decrementActionsCount}`}</div>
      <div>{counter.counterValue}</div>
      <button onClick={incrementOneHandler}>Increment 1</button>
      <button
        onClick={incrementNHandler}
      >{`Increment ${props.incrementStep}`}</button>
      <button onClick={decrementOneHandler}>Decrement 1</button>
      <button
        onClick={decrementNHandler}
      >{`Decrement ${props.incrementStep}`}</button>
      <button onClick={() => { dispatch({ type: RESET })}}>Reset</button>
    </div>
  );
};

export default Counter;
