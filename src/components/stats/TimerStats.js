import { useContext } from "react";
import StatsContext from "../../context/stats_context";

const TimerStats = () => {
    const { timersLaps, timersAmount } = useContext(StatsContext);
    return (
        <>
            <div>Timer laps: {timersLaps}</div>
            <div>Timer amount: 0</div>
        </>
    )
}

export default TimerStats;